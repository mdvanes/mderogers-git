/**
  @namespace Main namespace for scripts specific for the Rogers theme.
*/
var rogers = {};

/** 
  @namespace Utility functions for the Rogiers theme
*/
rogers.util = {};

/**
  @function rogers.util.selectivePageUpdate
  @description Updates a part of the page with the same part from a supplied string with new HTML.
  @param {String} updatedString The string containing the updated version of the HTML.
  @param {String} updatedTargetSelector The jQuery selector to find the element to be updated in the String.
*/
rogers.util.selectivePageUpdate = function(updatedString, updateTargetSelector) {
  var updatedSelection = jQuery(updateTargetSelector, updatedString);
  jQuery(updateTargetSelector).html( updatedSelection.html() );
}

/** 
  @namespace Functions that set bindings on elements.
*/
rogers.bindings = {};

/**
  @function rogers.bindings.ajaxLinks
  @description Ajax loading navigation for certain links.
*/
rogers.bindings.ajaxLinks = function() {
  jQuery("div#main").on("click", 
    "div.node-article h2 a, " + 
    "div.node-article li.node-readmore a, " +
    "div#breadcrumb a",
    function(event){
    // Do not follow the href
    event.preventDefault();
    console.log("ajaxLinks: " + this.href);
    // Do ajax update
    jQuery.ajax({
      url: this.href,
      success: function(data) {
        // Extract body contents from reply HTML document.
        var body = data.replace(/^[\S\s]*<body[^>]*?>/i, "").replace(/<\/body[\S\s]*$/i, "");
        // Add root tag
        body = "<div>" + body + "</div>";
        // Update parts of the page
        rogers.util.selectivePageUpdate(body, "div#main");
      }
    });
  });
}

/**
  @function rogers.init
  @description All scripts that must be run on page ready. Delegated to a function, so they can also be run at another time, if necessary.
*/
rogers.init = function() {
  rogers.bindings.ajaxLinks();
}

jQuery(document).ready(function(){
  rogers.init();
});