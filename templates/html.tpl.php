<?php
$htmlattrs = "version=\"HTML+RDFa 1.1\" xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:dc=\"http://purl.org/dc/terms/\" xmlns:sioc=\"http://rdfs.org/sioc/ns#\" lang=\"en\"";
/*
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN"
  "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" version="XHTML+RDFa 1.0" dir="ltr"
  xmlns:content="http://purl.org/rss/1.0/modules/content/"
  xmlns:dc="http://purl.org/dc/terms/"
  xmlns:foaf="http://xmlns.com/foaf/0.1/"
  xmlns:og="http://ogp.me/ns#"
  xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
  xmlns:sioc="http://rdfs.org/sioc/ns#"
  xmlns:sioct="http://rdfs.org/sioc/types#"
  xmlns:skos="http://www.w3.org/2004/02/skos/core#"
  xmlns:xsd="http://www.w3.org/2001/XMLSchema#">
Although prefix="" is more recent.
*/
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php print $htmlattrs; ?>> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" <?php print $htmlattrs; ?>> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" <?php print $htmlattrs; ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php print $htmlattrs; ?>> <!--<![endif]-->
  <head>
    <title><?php print $head_title; ?></title>
    <?php 
      /* $head: Markup for the HEAD section (including meta tags, keyword tags, and so on). */
      print $head; 

      /* $styles: Style tags necessary to import all CSS files for the page. */
      print $styles;

      /* $head_scripts: Script tags necessary to load the JavaScript files and settings for the page with scope head_scripts */
      print $head_scripts;
    ?>
  </head>
  <body>
<?php 
  /* $page_top: Initial markup from any modules that have altered the page. This variable should always be output first, before all other dynamic content.
     contains administrative links */
  print $page_top; 

  /* $page */
  print $page;

  /* $page_bottom: Final closing markup from any modules that have altered the page. This variable should always be output last, after all other dynamic content. */
  print $page_bottom; 

  /* $scripts: Script tags necessary to load the JavaScript files and settings for the page. */
  print $scripts;
?>

  <!-- Asynchronous Google Analytics snippet. Change UA-XXXXX-X to be your site's ID.
       mathiasbynens.be/notes/async-analytics-snippet -->
  <script>
    var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']];
    (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
    g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
    s.parentNode.insertBefore(g,s)}(document,'script'));
  </script>
<!-- 
start DEBUGGING
  $head_title_array: <?php print_r($head_title_array); ?> 
  $css: <?php print_r($css); ?>
  $language: <?php print_r($language); ?>
end DEBUGGING
-->
  </body>
</html>