<?php
/**
* Preprocesses the wrapping HTML.
*
* @param array &$variables
*   Template variables.
*/
function rogers_preprocess_html(&$vars) {
  
  /*
    <!-- <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> TODO should be added by template.php -->
    <meta name="description" content=""><!-- TODO -->
    <meta name="viewport" content="width=device-width">
    <meta charset="utf-8"/>
  */
  
  // See http://api.drupal.org/api/drupal/includes!common.inc/function/drupal_add_html_head/7

  // Setup meta tag for charset
  $meta_charset = array(
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => array(
      'charset' => 'utf-8'
    )
  );

  
  // Setup IE meta tag to force IE rendering mode
  $meta_ie_render_engine = array(
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => array(
      'content' =>  'IE=edge,chrome=1',
      'http-equiv' => 'X-UA-Compatible',
    )
  );
  
  // Setup meta tag for mobile device screens
  $meta_viewport = array(
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => array(
      'content' => 'width=device-width',
      'name' => 'viewport'
    )
  );
 
  // Add header meta tags to head
  drupal_add_html_head($meta_charset, 'meta_charset');
  drupal_add_html_head($meta_ie_render_engine, 'meta_ie_render_engine');
  drupal_add_html_head($meta_viewport, 'meta_viewport');
  
  // Some scripts really must be added in the 'head' element. Added with scope 'head_scripts'. 
  // The scripts that are added to the default scope are added at the bottom of the page.
  drupal_add_js(path_to_theme() . '/js/libs/modernizr-2.5.3.min.js',array('scope' => 'head_scripts'));
}

function rogers_process_html(&$vars) {
  $vars['head_scripts'] = drupal_get_js('head_scripts');
}

function rogers_js_alter(&$javascript) {
  // This function is an implementation of hook_js_alter
  
  // Grab Google CDN's jQuery, with a protocol relative URL; fall back to local if offline
  global $base_url;
  $jQuery_version = '1.9.1';
  $jQuery_local = $base_url.'/'.path_to_theme().'/jquery-'.$jQuery_version.'.min.js?v='.$jQuery_version;
  $jQuery_cdn = 'http://ajax.googleapis.com/ajax/libs/jquery/'.$jQuery_version.'/jquery.min.js';

  $javascript['misc/jquery.js']['data'] = $jQuery_cdn;
  $javascript['misc/jquery.js']['version'] = $jQuery_version;

  $group  = $javascript['misc/jquery.js']['group'] = JS_LIBRARY;
  $weight = $javascript['misc/jquery.js']['weight'] = -20;

  // drupal_add_js is appropriate here, because it is for the alteration of misc/jquery.js   
  drupal_add_js('window.jQuery || document.write(\'<script type="text/javascript" src="'.$jQuery_local.'"><\/script>\')',
    array('type'=>'inline', 'scope'=>'header', 'group'=>$group, 'every_page'=>TRUE, 'weight'=>$weight));
}
?>
